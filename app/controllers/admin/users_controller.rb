class Admin::UsersController < ApplicationController
  before_action :require_admin
  before_action :set_user, only: [:show, :edit, :update]

  # GET /admin/users
  # GET /admin/users.json
  def index
    @users = User.all.order(:id)
  end

  # GET /admin/users/1
  # GET /admin/users/1.json
  def show
  end

  # GET /admin/users/new
  def new
    @user = User.new
  end

  # GET /admin/users/1/edit
  def edit
  end

  # POST /admin/users
  # POST /admin/users.json
  def create
    @user = User.new(user_create_params)
    @user.password = SecureRandom.base64
    @user.password_confirmation = @user.password

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_user_url(@user),
                                  notice: 'User was successfully created.' }
        format.json { render :show,
                             status: :created,
                             location: admin_user_path(@user) }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/users/1
  # PATCH/PUT /admin/users/1.json
  def update
    respond_to do |format|
      _params = user_update_params
      if _params.delete(:access_locked) == "1"
        _params[:locked_at] = Time.now
      else
        _params[:locked_at] = nil
      end
      if @user.update(_params)
        format.html { redirect_to admin_user_url(@user),
                                  notice: 'User was successfully updated.' }
        format.json { render :show,
                             status: :ok,
                             location: admin_user_path(@user) }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_create_params
    params.require(:user).permit(:admin, :email, :name)
  end

  def user_update_params
    params.require(:user).permit(:access_locked, :admin, :name)
  end
end
