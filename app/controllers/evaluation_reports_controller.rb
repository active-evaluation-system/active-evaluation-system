class EvaluationReportsController < ApplicationController
  before_action :require_admin, except: [:show]
  before_action :set_evaluation_report, only: [:show, :edit, :update, :destroy]
  before_action :set_evaluation_target, only: [:new, :create]
  before_action :set_evaluation_targets, only: [:new, :show, :edit, :update]

  # GET /evaluation_reports/1
  # GET /evaluation_reports/1.json
  def show
  end

  # GET /evaluation_targets/1/evaluation_reports/new
  def new
    @evaluation_report = @evaluation_target.build_evaluation_report
  end

  # GET /evaluation_reports/1/edit
  def edit
  end

  # POST /evaluation_targets/1/evaluation_reports
  def create
    @evaluation_report =
      @evaluation_target.build_evaluation_report(evaluation_report_params)

    respond_to do |format|
      if @evaluation_report.save
        format.html { redirect_to @evaluation_report, notice: 'Evaluation report was successfully updated.' }
        format.json { render :show, status: :ok, location: @evaluation_report }
      else
        format.html { render :new }
        format.json { render json: @evaluation_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evaluation_reports/1
  # PATCH/PUT /evaluation_reports/1.json
  def update
    respond_to do |format|
      if @evaluation_report.update(evaluation_report_params)
        format.html { redirect_to @evaluation_report, notice: 'Evaluation report was successfully updated.' }
        format.json { render :show, status: :ok, location: @evaluation_report }
      else
        format.html { render :edit }
        format.json { render json: @evaluation_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluation_reports/1
  # DELETE /evaluation_reports/1.json
  def destroy
    period = @evaluation_report.period
    @evaluation_report.destroy
    respond_to do |format|
      format.html { redirect_to period, notice: 'Evaluation report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evaluation_report
      @evaluation_report =
        EvaluationReport.
          eager_load(evaluation_target: [
                       :period,
                       :evaluatee,
                       objectives: [:item_tag],
                     ]).
          find(params[:id])
      @evaluation_target = @evaluation_report.evaluation_target
    end

    def set_evaluation_target
      @evaluation_target = EvaluationTarget.find(params[:evaluation_target_id])
    end

    def set_evaluation_targets
      @period = @evaluation_target.period
      @evaluatee = @evaluation_target.evaluatee
      @objectives = @evaluation_target.objectives.eager_load(:item_tag)
      @evaluation_targets =
        @evaluatee.
          evaluation_targets.
          eager_load(:period, :evaluation_report).
          order("periods.start_on" => :asc)
      index = @evaluation_targets.index(@evaluation_target)
      if index.zero?
        @previous_evaluation_target = nil
      else
        @previous_evaluation_target = @evaluation_targets[index - 1]
      end

      @period_summary_values = @evaluatee.all_periods_final_summary_values
      @grouped_final_evaluations =
        @evaluation_target.grouped_final_evaluations
      set_previous_grouped_final_evaluations
    end

    def set_previous_grouped_final_evaluations
      # Convert @previous_evaluation_target.grouped_final_evaluations
      # for easy to join @evaluation_target.grouped_final_evaluations.
      #
      # @previous_evaluation_target.grouped_final_evaluations:
      # {
      #    item_tag_id => [final_evaluation, ...]
      # }
      # ->
      # {
      #   item_tag_id => {
      #     item_id => final_evaluation,
      #     ...,
      #   },
      #   ...
      # }
      @previous_grouped_final_evaluations =
        @previous_evaluation_target&.grouped_final_evaluations || {}
      @previous_grouped_final_evaluations.each do |item_tag_id, final_evaluations|
        item_to_final_evaluation = {}
        final_evaluations.each do |final_evaluation|
          item_to_final_evaluation[final_evaluation.item_id] = final_evaluation
        end
        @previous_grouped_final_evaluations[item_tag_id] = item_to_final_evaluation
      end
    end

    # Only allow a list of trusted parameters through.
    def evaluation_report_params
      params.require(:evaluation_report).permit(:content)
    end
end
