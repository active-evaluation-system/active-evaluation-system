class AssignmentsController < ApplicationController
  before_action :require_admin
  before_action :set_evaluation_target, only: [:create]
  before_action :set_assignment, only: [:destroy]

  # POST /evaluation_targets/1/assignments
  # POST /evaluation_targets/1/assignments.json
  def create
    @assignment = Assignment.new(assignment_params)
    @assignment.evaluation_target = @evaluation_target

    respond_to do |format|
      if @assignment.save
        format.html do
          redirect_to @evaluation_target,
                      notice: 'Assignment was successfully created.'
        end
        format.json { render :show, status: :created, location: @assignment }
      else
        format.html do
          redirect_to @evaluation_target,
                      notice: "Assignment was not successfully created: #{@assignment.errors.full_messages.join("\n")}"
        end
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assignments/1
  # DELETE /assignments/1.json
  def destroy
    @evaluation_target = @assignment.evaluation_target
    @assignment.destroy
    respond_to do |format|
      format.html { redirect_to @evaluation_target, notice: 'Assignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_evaluation_target
    @evaluation_target = EvaluationTarget.find(params[:evaluation_target_id])
  end

  def set_assignment
    @assignment = Assignment.find(params[:id])
    @evaluation_target = @assignment.evaluation_target
  end

  def assignment_params
    params.require(:assignment).permit(:evaluator_id)
  end
end
