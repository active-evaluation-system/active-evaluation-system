class FinalEvaluationsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_evaluation_target, only: [:synchronize, :index]
  before_action :set_final_evaluation, only: [:show, :edit, :update]
  before_action :set_final_evaluation_in_previous_period,
                only: [:show, :edit, :update]

  # POST /evaluation_target/1/final_evaluations
  # POST /evaluation_target/1/final_evaluations.json
  def synchronize
    items = @evaluation_target.items
    FinalEvaluation.
      where(evaluation_target: @evaluation_target).
      where.not(item: items).
      destroy_all
    items.each do |item|
      FinalEvaluation.create_or_find_by(evaluation_target: @evaluation_target,
                                        item: item)
    end
    redirect_to evaluation_target_final_evaluations_url(@evaluation_target)
  end

  # GET /evaluation_target/1/final_evaluations
  # GET /evaluation_target/1/final_evaluations.json
  def index
    @items = @evaluation_target.sorted_items
    @final_evaluations = @evaluation_target.sorted_final_evaluations
  end

  # GET /final_evaluations/1
  # GET /final_evaluations/1.json
  def show
  end

  # GET /final_evaluations/1/edit
  def edit
  end

  # PATCH/PUT /final_evaluations/1
  # PATCH/PUT /final_evaluations/1.json
  def update
    respond_to do |format|
      if @final_evaluation.update(final_evaluation_params)
        format.html do
          if @next_final_evaluation
            redirect_to edit_final_evaluation_url(@next_final_evaluation),
                        notice: 'Final evaluation was successfully updated.'
          else
            redirect_to @period,
                        notice: 'Final evaluation was successfully updated.'
          end
        end
        format.json { render :show, status: :ok, location: @final_evaluation }
      else
        format.html { render :edit }
        format.json { render json: @final_evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_evaluation_target
      @evaluation_target = EvaluationTarget.find(params[:evaluation_target_id])
      @period = @evaluation_target.period
      @evaluatee = @evaluation_target.evaluatee
    end

    def set_final_evaluation
      @final_evaluation =
        FinalEvaluation.
          eager_load(evaluation_target: [:period, :evaluatee],
                     item: []).
          find(params[:id])
      @evaluation_target = @final_evaluation.evaluation_target
      @period = @final_evaluation.period
      @evaluatee = @final_evaluation.evaluatee
      @item = @final_evaluation.item
      @evaluations =
        Evaluation.
          includes(evaluation_target: [:evaluatee, :period],
                   assignment: [:evaluator]).
          where(assignments: {
                  evaluation_target: @evaluation_target,
                },
                item: @item).
          order("assignments.evaluator_id")
      @items = @evaluation_target.sorted_items
      index = @items.index(@item)
      if index
        if index.zero?
          @previous_final_evaluation = nil
        else
          @previous_final_evaluation =
            FinalEvaluation.find_by(evaluation_target: @evaluation_target,
                                    item: @items[index - 1])
        end
        if index == @items.size - 1
          @next_final_evaluation = nil
        else
          @next_final_evaluation =
            FinalEvaluation.find_by(evaluation_target: @evaluation_target,
                                    item: @items[index + 1])
        end
      else
        @previous_final_evaluation = nil
        @next_final_evaluation = nil
      end
    end

    def set_final_evaluation_in_previous_period
      @final_evaluation_in_previous_period =
        FinalEvaluation.
          eager_load(evaluation_target: [:period, :evaluatee],
                     item: []).
          where(item: @final_evaluation.item).
          where(evaluation_target: @evaluatee.
                  evaluation_targets.
                  eager_load(:period).
                  where(periods: {start_on: ...@period.start_on}).
                  order("periods.start_on" => :desc).
                  limit(1)).
          first
    end

    # Only allow a list of trusted parameters through.
    def final_evaluation_params
      params.require(:final_evaluation).permit(:value, :comment)
    end
end
