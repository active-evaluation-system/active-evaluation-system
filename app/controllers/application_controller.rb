class ApplicationController < ActionController::Base
  around_action :switch_locale
  before_action :authenticate_user!

  private
  def switch_locale(&action)
    locale = "ja" # TODO
    I18n.with_locale(locale, &action)
  end

  def require_admin
    return if current_user.admin?
    redirect_to root_url, notice: 'Not an administrator user'
  end
end
