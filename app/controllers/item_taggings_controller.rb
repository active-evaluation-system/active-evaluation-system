class ItemTaggingsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_item_tagging, only: [:show, :edit, :update, :destroy]

  # GET /item_taggings
  # GET /item_taggings.json
  def index
    @item_taggings = ItemTagging.all
  end

  # GET /item_taggings/1
  # GET /item_taggings/1.json
  def show
  end

  # GET /item_taggings/new
  def new
    @item_tagging = ItemTagging.new
  end

  # GET /item_taggings/1/edit
  def edit
  end

  # POST /item_taggings
  # POST /item_taggings.json
  def create
    @item_tagging = ItemTagging.new(item_tagging_params)

    respond_to do |format|
      if @item_tagging.save
        format.html { redirect_to @item_tagging, notice: 'Item tagging was successfully created.' }
        format.json { render :show, status: :created, location: @item_tagging }
      else
        format.html { render :new }
        format.json { render json: @item_tagging.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_taggings/1
  # PATCH/PUT /item_taggings/1.json
  def update
    respond_to do |format|
      if @item_tagging.update(item_tagging_params)
        format.html { redirect_to @item_tagging, notice: 'Item tagging was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_tagging }
      else
        format.html { render :edit }
        format.json { render json: @item_tagging.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_taggings/1
  # DELETE /item_taggings/1.json
  def destroy
    @item_tagging.destroy
    respond_to do |format|
      format.html { redirect_to item_taggings_url, notice: 'Item tagging was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_tagging
      @item_tagging = ItemTagging.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def item_tagging_params
      params.require(:item_tagging).permit(:item_id, :item_tag_id)
    end
end
