class StandardizationsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_standardization, only: [:show, :edit, :update, :destroy]

  # GET /standardizations
  # GET /standardizations.json
  def index
    @standardizations = Standardization.all
  end

  # GET /standardizations/1
  # GET /standardizations/1.json
  def show
  end

  # GET /standardizations/new
  def new
    @standardization = Standardization.new
  end

  # GET /standardizations/1/edit
  def edit
  end

  # POST /standardizations
  # POST /standardizations.json
  def create
    @standardization = Standardization.new(standardization_params)

    respond_to do |format|
      if @standardization.save
        format.html { redirect_to @standardization, notice: 'Standardization was successfully created.' }
        format.json { render :show, status: :created, location: @standardization }
      else
        format.html { render :new }
        format.json { render json: @standardization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /standardizations/1
  # PATCH/PUT /standardizations/1.json
  def update
    respond_to do |format|
      if @standardization.update(standardization_params)
        format.html { redirect_to @standardization, notice: 'Standardization was successfully updated.' }
        format.json { render :show, status: :ok, location: @standardization }
      else
        format.html { render :edit }
        format.json { render json: @standardization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /standardizations/1
  # DELETE /standardizations/1.json
  def destroy
    @standardization.destroy
    respond_to do |format|
      format.html { redirect_to standardizations_url, notice: 'Standardization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_standardization
      @standardization = Standardization.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def standardization_params
      params.require(:standardization).permit(:standard_id, :item_id)
    end
end
