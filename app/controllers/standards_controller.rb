class StandardsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_standard, only: [:show, :edit, :update, :destroy]

  # GET /standards
  # GET /standards.json
  def index
    @standards = Standard.order(name: :desc)
  end

  # GET /standards/1
  # GET /standards/1.json
  def show
    # TODO: This assumes that newer standard name is greater than
    # previous standard names. But it's not a good design.
    @previous_standard = Standard.
                           where(name: ...@standard.name).
                           order(name: :desc).
                           limit(1).
                           first
    if @previous_standard
      @added_items = @standard.items - @previous_standard.items
      @deleted_items = @previous_standard.items - @standard.items
    else
      @added_items = []
      @deleted_items = []
    end
  end

  # GET /standards/new
  def new
    @standard = Standard.new
  end

  # GET /standards/1/edit
  def edit
  end

  # POST /standards
  # POST /standards.json
  def create
    @standard = Standard.new(standard_params)

    respond_to do |format|
      if @standard.save
        format.html { redirect_to @standard, notice: 'Standard was successfully created.' }
        format.json { render :show, status: :created, location: @standard }
      else
        format.html { render :new }
        format.json { render json: @standard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /standards/1
  # PATCH/PUT /standards/1.json
  def update
    respond_to do |format|
      if @standard.update(standard_params)
        format.html { redirect_to @standard, notice: 'Standard was successfully updated.' }
        format.json { render :show, status: :ok, location: @standard }
      else
        format.html { render :edit }
        format.json { render json: @standard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /standards/1
  # DELETE /standards/1.json
  def destroy
    @standard.destroy
    respond_to do |format|
      format.html { redirect_to standards_url, notice: 'Standard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_standard
      @standard = Standard.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def standard_params
      params.require(:standard).permit(:name,
                                       :description,
                                       {
                                         standardizations_attributes: [
                                           :id,
                                           :item_id,
                                           :standard_id,
                                           :_destroy,
                                         ],
                                       })
    end
end
