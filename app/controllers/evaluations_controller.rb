class EvaluationsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_assignment, only: [:synchronize, :index]
  before_action :set_evaluation, only: [:show, :edit, :update]
  before_action :set_evaluation_in_previous_period, only: [:show, :edit, :update]

  # POST /assignments/1/evaluations
  # POST /assignments/1/evaluations.json
  def synchronize
    items = @assignment.evaluation_target.items
    Evaluation.
      where(assignment: @assignment).
      where.not(item: items).
      destroy_all
    items.each do |item|
      Evaluation.find_or_create_by!(assignment: @assignment, item: item)
    end
    redirect_to assignment_evaluations_url(@assignment)
  end

  # GET /assignments/1/evaluations
  # GET /assignments/1/evaluations.json
  def index
    @items = @assignment.evaluation_target.sorted_items
    @evaluations =
      Evaluation.
        eager_load(item: :item_tags).
        where(assignment: @assignment).
        sort_by do |e|
      @items.index(e.item) || @items.size
    end
  end

  # GET /evaluations/1
  # GET /evaluations/1.json
  def show
  end

  # GET /evaluations/1/edit
  def edit
  end

  # PATCH/PUT /evaluations/1
  # PATCH/PUT /evaluations/1.json
  def update
    respond_to do |format|
      if @evaluation.update(evaluation_params)
        format.html do
          if @next_evaluation
            redirect_to edit_evaluation_url(@next_evaluation),
                        notice: 'Evaluation was successfully updated.'
          else
            redirect_to @assignment.period,
                        notice: 'Final evaluation was successfully updated.'
          end
        end
        format.json { render :show, status: :ok, location: @evaluation }
      else
        format.html { render :edit }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_assignment
      @assignment = Assignment.find(params[:assignment_id])
    end

    def set_evaluation
      @evaluation = Evaluation.find(params[:id])
      @assignment = @evaluation.assignment
      @period = @assignment.period
      @item = @evaluation.item
      @items = @assignment.evaluation_target.sorted_items
      index = @items.index(@item)
      if index.zero?
        @previous_evaluation = nil
      else
        @previous_evaluation = Evaluation.find_by(assignment: @assignment,
                                                  item: @items[index - 1])
      end
      @next_evaluation = Evaluation.find_by(assignment: @assignment,
                                            item: @items[index + 1])
    end

    def set_evaluation_in_previous_period
      @evaluation_in_previous_period =
        Evaluation.
          eager_load(assignment: [:period, :evaluatee],
                     item: []).
          where(item: @evaluation.item).
          where(assignment: {
                  evaluation_target: @assignment.
                    evaluatee.
                    evaluation_targets.
                    eager_load(:period).
                    where(periods: {start_on: ...@period.start_on}).
                    order("periods.start_on" => :desc).
                    limit(1)
                }).
          first
    end

    # Only allow a list of trusted parameters through.
    def evaluation_params
      params.require(:evaluation).permit(:value, :comment)
    end
end
