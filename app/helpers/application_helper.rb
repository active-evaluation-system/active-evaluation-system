module ApplicationHelper
  def render_markdown(markdown)
    return "" if markdown.blank?
    raw(MarkdownRenderer.new(markdown).render)
  end

  def current_period_class(condition)
    if condition
      "current-period"
    else
      ""
    end
  end
end
