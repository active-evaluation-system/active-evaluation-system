class MarkdownRenderer
  class Linker
    include ActionView::Helpers
    include ActionDispatch::Routing
    include Rails.application.routes.url_helpers
  end

  if Object.const_defined?(:CommonMarker)
    # commonmarker < 1.0.0
    class Renderer < CommonMarker::HtmlRenderer
      def text(node)
        out(format_text(node.string_content))
      end

      private
      def linker
        @linker ||= Linker.new
      end

      def format_text(text)
        formatted_text = ""
        text.split(/((?:event|item-tag|item)#\d+)/).each do |chunk|
          case chunk
          when /\Aevent#(\d+)\z/
            event = Event.find(Integer($1, 10))
            label = "#{event.id}: #{event.title}"
            formatted_text << linker.link_to(label, event, title: label)
          when /\Aitem-tag#(\d+)\z/
            item_tag = ItemTag.find(Integer($1, 10))
            label = "#{item_tag.id}: #{item_tag.name}"
            formatted_text << linker.link_to(label, item_tag, title: label)
          when /\Aitem#(\d+)\z/
            item = Item.find(Integer($1, 10))
            label = "#{item.id}: #{item.name}"
            formatted_text << linker.link_to(label, item, title: label)
          else
            formatted_text << escape_html(chunk).force_encoding(chunk.encoding)
          end
        end
        formatted_text
      end
    end

    def initialize(markdown)
      @markdown = markdown
      @extensions = [
        :table,
        :tasklist,
        :strikethrough,
        :autolink,
        :tagfilter,
      ]
    end

    def render
      doc = CommonMarker.render_doc(@markdown, :DEFAULT, @extensions)
      renderer = Renderer.new(options: :DEFAULT,
                              extensions: @extensions)
      renderer.render(doc)
    end
  else
    def initialize(markdown)
      @markdown = markdown
      @linker = Linker.new
    end

    def render
      doc = Commonmarker.parse(@markdown,
                               options: {
                                 extension: {footnotes: true},
                               })
      doc.walk do |node|
        next unless node.type == :text
        next if node.parent.type == :link
        format_text(node)
      end
      doc.to_html
    end

    private
    def format_text(text_node)
      need_replace = false
      contents = []
      text_node.string_content.split(/((?:event|item-tag|item)#\d+)/).each do |chunk|
        case chunk
        when /\Aevent#(\d+)\z/
          need_replace = true
          event = Event.find(Integer($1, 10))
          label = "#{event.id}: #{event.title}"
          contents << create_link_node(@linker.url_for(event), label)
        when /\Aitem-tag#(\d+)\z/
          need_replace = true
          item_tag = ItemTag.find(Integer($1, 10))
          label = "#{item_tag.id}: #{item_tag.name}"
          contents << create_link_node(@linker.url_for(item_tag), label)
        when /\Aitem#(\d+)\z/
          need_replace = true
          item = Item.find(Integer($1, 10))
          label = "#{item.id}: #{item.name}"
          contents << create_link_node(@linker.url_for(item), label)
        else
          contents << create_text_node(chunk)
        end
      end
      return unless need_replace

      contents.each do |content|
        text_node.insert_before(content)
      end
      text_node.delete
    end

    def create_link_node(url, title)
      node = Commonmarker::Node.new(:link, url: url, title: title)
      node.append_child(create_text_node(title))
      node
    end

    def create_text_node(text)
      node = Commonmarker::Node.new(:text)
      node.string_content = text
      node
    end
  end
end
