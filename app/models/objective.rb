class Objective < ApplicationRecord
  belongs_to :evaluation_target
  belongs_to :item_tag
end
