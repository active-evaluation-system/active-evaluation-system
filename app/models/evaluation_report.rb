class EvaluationReport < ApplicationRecord
  belongs_to :evaluation_target
  has_one :period, through: :evaluation_target
  has_one :evaluatee, through: :evaluation_target
  has_many :objectives, through: :evaluation_target

  def find_final_evaluations(items)
    FinalEvaluation.where(evaluation_target_id: evaluation_target_id,
                          item: items)
  end
end
