class EvaluatedItem
  attr_reader :period
  attr_reader :item
  attr_reader :evaluatee

  def initialize(period, item, evaluatee=nil)
    @period = period
    @item = item
    @evaluatee = evaluatee
  end

  def find_evaluation_or_initialize(assignment_id, item_id)
    ensure_evaluations
    @evaluations_index[[assigment_id, item_id]] ||
      Evaluation.new(assignment_id: assignment_id, item_id: item_id)
  end

  def evaluatees
    @evaluatees ||= @period.evaluatees.distinct.order(:id)
  end

  def evaluators
    @evaluators ||= @period.evaluators.distinct.order(:id)
  end

  def evaluations
    ensure_evaluations
    @evaluations
  end

  private def find_assignments
    assignments = @period.assignments
    assignments = assignments.where(evaluatee: @evaluatee) if @evaluatee
    assignments
  end

  private def ensure_evaluations
    @evaluations_index = {}
    @evaluations = {}
    target_evaluations = Evaluation.where(assignment: find_assignments,
                                          item: @item)
    target_evaluations.
      eager_load(assignment: [:evaluatee, :evaluator]).
      each do |evaluation|
      assignment = evaluation.assignment
      @evaluations[assignment.evaluatee] ||= {}
      @evaluations[assignment.evaluatee][assignment.evaluator] = evaluation
      @evaluations_index[[evaluation.assignment_id, evaluation.item_id]] =
        evaluation
    end
    find_assignments.eager_load(:evaluatee, :evaluator).each do |assignment|
      @evaluations[assignment.evaluatee] ||= {}
      @evaluations[assignment.evaluatee][assignment.evaluator] ||=
        Evaluation.new(assignment: assignment,
                       item: @item,
                       value: 0)
    end
    @evaluations.transform_values! do |evaluations|
      evaluations.values.sort_by do |evaluation|
        evaluation.assignment.evaluator.id
      end
    end
    @evaluations
  end

  def final_evaluation
    @final_evaluation ||= find_final_evaluation
  end

  private def find_final_evaluation
    @period.
      final_evaluations.
      where(item: @item, evaluatee: @evaluatee).
      first
  end

  def final_evaluations
    @final_evaluations ||= find_final_evaluations
  end

  private def find_final_evaluations
    final_evaluations = {}
    @period.
      final_evaluations.
      where(item: @item).
      eager_load(:evaluatee).
      each do |final_evaluation|
      final_evaluations[final_evaluation.evaluatee] = final_evaluation
    end
    find_assignments.eager_load(:evaluatee).each do |assignment|
      final_evaluations[assignment.evaluatee] ||=
        FinalEvaluation.new(period: period,
                            evaluatee: assignment.evaluatee,
                            item: @item,
                            value: 0)
    end
    final_evaluations
  end
end
