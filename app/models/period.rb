class Period < ApplicationRecord
  belongs_to :standard
  has_many :evaluation_targets,
           dependent: :destroy
  has_many :assignments,
           through: :evaluation_targets
  has_many :evaluatees,
           -> {distinct.order(:id)},
           through: :evaluation_targets
  has_many :evaluators,
           -> {distinct.order(:id)},
           through: :assignments
  has_many :final_evaluations,
           through: :evaluation_targets,
           dependent: :destroy
  has_many :evaluation_reports,
           through: :evaluation_targets,
           dependent: :destroy

  def events
    Event.
      where(occurred_at: start_on..end_on).
      order(occurred_at: :asc)
  end
end
