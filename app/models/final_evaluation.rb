class FinalEvaluation < ApplicationRecord
  belongs_to :evaluation_target
  belongs_to :item
  has_one :period, through: :evaluation_target
  has_one :evaluatee, through: :evaluation_target

  def good?
    value >= 0.75
  end

  def events
    period.events.joins(:participants).where(participants: {user: evaluatee})
  end
end
