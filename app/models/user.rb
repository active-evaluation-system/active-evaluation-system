class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :timeoutable and :omniauthable
  devise :confirmable,
         :database_authenticatable,
         :lockable,
         :recoverable,
         # TODO
         # :registerable,
         :rememberable,
         :trackable,
         :validatable,
         lock_strategy: :none,
         unlock_strategy: :none

  has_many :evaluation_targets,
           foreign_key: "evaluatee_id",
           dependent: :destroy
  has_many :evaluation_reports,
           through: :evaluation_targets
  has_many :evaluatee_periods,
           -> {distinct},
           through: :evaluation_targets,
           source: :period,
           class_name: "Period"
  has_many :evaluator_assignments,
           foreign_key: "evaluator_id",
           class_name: "Assignment",
           dependent: :destroy
  has_many :evaluator_periods,
           -> {distinct},
           through: :evaluator_assignments,
           source: :period,
           class_name: "Period"
  has_many :participants, dependent: :destroy
  has_many :events, through: :participants

  scope :active, -> { where(locked_at: nil) }

  def initialize(*args)
    super
    if User.count.zero?
      self.admin = true
    else
      self.locked_at = Time.now.utc
    end
  end

  def display_name
    name || email[/\A[^@]+/, 0]
  end

  def all_periods_final_summary_values
    all_periods_summary_values = {}
    FinalEvaluation.
      joins(evaluation_target: [], item: :item_tags).
      where(evaluation_target: {evaluatee: self}).
      group("evaluation_target_id",
            "item_tag_id").
      select("evaluation_target_id",
             "item_tag_id",
             "sum(value) as total_value").
      each do |final_evaluation|
      target = final_evaluation.evaluation_target
      summary_values = (all_periods_summary_values[target.period_id] ||= {})
      summary_values[final_evaluation.item_tag_id] = final_evaluation.total_value
    end
    all_periods_summary_values
  end
end
