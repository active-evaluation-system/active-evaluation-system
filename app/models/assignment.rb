class Assignment < ApplicationRecord
  belongs_to :evaluation_target
  has_one :evaluatee, through: :evaluation_target
  has_one :period, through: :evaluation_target
  belongs_to :evaluator, class_name: "User"
  has_many :evaluations, dependent: :destroy
end
