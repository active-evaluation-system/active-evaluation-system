json.extract! evaluation_report, :id, :period_id, :evaluatee_id, :content, :created_at, :updated_at
json.url evaluation_report_url(evaluation_report, format: :json)
