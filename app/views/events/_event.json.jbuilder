json.extract! event, :id, :title, :url, :occurred_at, :description, :created_at, :updated_at
json.url event_url(event, format: :json)
