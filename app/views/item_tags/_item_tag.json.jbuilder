json.extract! item_tag, :id, :name, :created_at, :updated_at
json.url item_tag_url(item_tag, format: :json)
