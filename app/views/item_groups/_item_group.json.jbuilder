json.extract! item_group, :id, :name, :created_at, :updated_at
json.url item_group_url(item_group, format: :json)
