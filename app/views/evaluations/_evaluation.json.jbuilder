json.extract! evaluation, :id, :assignment_id, :item_id, :value, :comment, :created_at, :updated_at
json.url evaluation_url(evaluation, format: :json)
