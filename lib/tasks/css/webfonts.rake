namespace :css do
  desc "Copy webfonts"
  task :webfonts do
    webfonts_dir = "app/assets/builds/webfonts"
    rm_rf(webfonts_dir)
    mkdir_p(webfonts_dir)
    Dir.glob("node_modules/@fortawesome/fontawesome-free/webfonts/*.*") do |path|
      cp(path, webfonts_dir)
    end
  end
  task :build => :webfonts
end
