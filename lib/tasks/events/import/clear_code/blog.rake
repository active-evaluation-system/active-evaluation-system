namespace :events do
  namespace :import do
    namespace :clear_code do
      desc "Import blog posts of ClearCode Inc. as events"
      task blog: :environment do
        user_mapping = {
          "abetomo" => "abe",
          "daipom" => "fukuda",
          "kenhys" => "hayashi",
          "komainu8" => "horimoto",
          "piro_or" => "yuki",
          "yashirot" => "yashiro",
          "yoshimotoy" => "yoshimoto",
        }

        format_link_url = lambda do |path|
          "https://www.clear-code.com/" + path.gsub(/\.md\z/, ".html")
        end

        extract_date = lambda do |basename|
          Date.parse(basename[/\A\d+-\d+-\d+/])
        end

        format_blog_url = lambda do |basename|
          date = extract_date.call(basename)
          slug = basename.gsub(/\A\d+-\d+-\d+-/, "")
          "https://www.clear-code.com/blog/" +
            "#{date.year}/#{date.month}/#{date.day}/#{slug}.html"
        end

        format_excerpt = lambda do |text|
          text.gsub(/{%\s*(link|post_url)\s+(.+?)\s*%}/) do
            type = $1
            case type
            when "link"
              path = $2
              format_link_url.call(path)
            when "post_url"
              basename = $2
              format_blog_url.call(basename)
            end
          end
        end

        Dir.mktmpdir do |dir|
          website_dir = File.join(dir, "website")
          sh("git",
             "clone",
             "--depth", "30",
             "https://gitlab.com/clear-code/website.git",
             website_dir)

          config = YAML.load(File.read(File.join(website_dir, "_config.yml")))
          excerpt_separator = config["excerpt_separator"]

          n_found_events = 0
          max_n_found_events = 5
          Dir.glob("_posts/*.md", base: website_dir).reverse_each do |md|
            basename = File.basename(md, ".md")
            md = File.join(website_dir, md)
            content = File.read(md)
            _, preamble, body = content.split(/^---\s*$/, 3)
            metadata = YAML.load(preamble)
            excerpt = body.split(excerpt_separator, 2)[0]
            url = format_blog_url.call(basename)
            event = Event.find_or_initialize_by(url: url)
            if event.persisted?
              n_found_events += 1
              break if n_found_events == max_n_found_events
              next
            end

            event.title = "blog: #{metadata["title"]}"
            event.description = format_excerpt.call(excerpt)
            event.occurred_at = extract_date.call(basename)
            event.save!
            author = metadata["author"]
            local_name = user_mapping[author] || author
            user = User.find_by(email: "#{local_name}@clear-code.com")
            if user.nil?
              raise "Unknown user mapping: " +
                    "#{local_name}: #{website_user_mapping.inspect}"
            end
            event.participants.create!(user: user)
          end
        end
      end
    end
  end
end
