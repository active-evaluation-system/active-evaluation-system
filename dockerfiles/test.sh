#!/bin/sh

set -eux

git config --global --add safe.directory /source/.git
git clone /source/.git source
cd source
PATH=$PWD/bin:$PATH

bundle config path ../vendor/bundle
bundle install
rails yarn:install
rails css:build
rails db:create || :
rails db:migrate
# Active Record creates "pgroonga" schema that is created by "CREATE
# EXTENSION pgroonga" in db/schema.rb but it should not be done. If we
# create "pgroonga" schema manually, "CREATE EXTENSION pgroonga" is
# failed. We can remove this workaround with PGroonga 4. PGroonga 4
# will not create the deprecated "pgroogna" schema.
sed -i -e '/^  create_schema "pgroonga"/d' db/schema.rb
rails test
rails db:migrate VERSION=0
rails db:migrate
