class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :name
      t.text :reason
      t.text :example

      t.timestamps
    end
  end
end
