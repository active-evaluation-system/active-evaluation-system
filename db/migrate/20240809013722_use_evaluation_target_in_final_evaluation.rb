class UseEvaluationTargetInFinalEvaluation < ActiveRecord::Migration[7.1]
  class FinalEvaluation < ActiveRecord::Base
  end

  class EvaluationTarget < ActiveRecord::Base
  end

  def up
    add_reference :final_evaluations, :evaluation_target, null: true, foreign_key: true

    FinalEvaluation.all.find_each do |evaluation|
      evaluation_target =
        EvaluationTarget.find_or_create_by!(period_id: evaluation.period_id,
                                            evaluatee_id: evaluation.evaluatee_id)
      evaluation.evaluation_target_id = evaluation_target.id
      evaluation.save!
    end

    change_column_null :final_evaluations, :evaluation_target_id, false
    add_index :final_evaluations, [:evaluation_target_id, :item_id], unique: true
    remove_column :final_evaluations, :period_id
    remove_column :final_evaluations, :evaluatee_id
  end

  def down
    add_reference :final_evaluations, :period, null: true, foreign_key: true
    add_reference :final_evaluations, :evaluatee, null: true, foreign_key: { to_table: :users }

    FinalEvaluation.all.find_each do |evaluation|
      evaluation_target = EvaluationTarget.find(evaluation.evaluation_target_id)
      evaluation.period_id = evaluation_target.period_id
      evaluation.evaluatee_id = evaluation_target.evaluatee_id
      evaluation.save!
    end

    change_column_null :final_evaluations, :period_id, false
    change_column_null :final_evaluations, :evaluatee_id, false
    remove_column :final_evaluations, :evaluation_target_id
  end
end
