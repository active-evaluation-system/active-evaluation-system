class UseEvaluationTargetInEvaluationReport < ActiveRecord::Migration[7.1]
  class EvaluationReport < ActiveRecord::Base
  end

  class EvaluationTarget < ActiveRecord::Base
  end

  def up
    add_reference :evaluation_reports, :evaluation_target, null: true, foreign_key: true

    EvaluationReport.all.find_each do |report|
      evaluation_target =
        EvaluationTarget.find_or_create_by!(period_id: report.period_id,
                                            evaluatee_id: report.evaluatee_id)
      report.evaluation_target_id = evaluation_target.id
      report.save!
    end

    change_column_null :evaluation_reports, :evaluation_target_id, false
    remove_index :evaluation_reports, :evaluation_target_id
    add_index :evaluation_reports, :evaluation_target_id, unique: true
    remove_column :evaluation_reports, :period_id
    remove_column :evaluation_reports, :evaluatee_id
  end

  def down
    add_reference :evaluation_reports, :period, null: true, foreign_key: true
    add_reference :evaluation_reports, :evaluatee, null: true, foreign_key: { to_table: :users }

    EvaluationReport.all.find_each do |report|
      evaluation_target = EvaluationTarget.find(report.evaluation_target_id)
      report.period_id = evaluation_target.period_id
      report.evaluatee_id = evaluation_target.evaluatee_id
      report.save!
    end

    change_column_null :evaluation_reports, :period_id, false
    change_column_null :evaluation_reports, :evaluatee_id, false
    remove_column :evaluation_reports, :evaluation_target_id
  end
end
