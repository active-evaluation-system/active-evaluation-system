class CreateFinalEvaluations < ActiveRecord::Migration[6.0]
  def change
    create_table :final_evaluations do |t|
      t.references :period, null: false, foreign_key: true
      t.references :evaluatee, null: false, foreign_key: { to_table: :users }
      t.references :item, null: false, foreign_key: true
      t.float :value
      t.text :comment

      t.timestamps
    end

    add_index :final_evaluations, :value
  end
end
