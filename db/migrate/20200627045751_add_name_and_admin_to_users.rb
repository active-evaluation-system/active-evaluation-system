class AddNameAndAdminToUsers < ActiveRecord::Migration[6.0]
  def change
    change_table :users do |t|
      t.string :name
      t.boolean :admin
    end

    add_index :users, :admin
  end
end
