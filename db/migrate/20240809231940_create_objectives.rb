class CreateObjectives < ActiveRecord::Migration[7.1]
  def change
    create_table :objectives do |t|
      t.references :evaluation_target, null: false, foreign_key: true
      t.references :item_tag, null: false, foreign_key: true

      t.timestamps

      t.index [:evaluation_target_id, :item_tag_id], unique: true
    end
  end
end
