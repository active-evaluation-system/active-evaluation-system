class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :title
      t.string :url
      t.datetime :occurred_at
      t.text :description

      t.timestamps
    end

    add_index :events, :occurred_at
  end
end
