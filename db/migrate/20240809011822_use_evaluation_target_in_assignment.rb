class UseEvaluationTargetInAssignment < ActiveRecord::Migration[7.1]
  class Assignment < ActiveRecord::Base
  end

  class EvaluationTarget < ActiveRecord::Base
  end

  def up
    add_reference :assignments, :evaluation_target, null: true, foreign_key: true

    Assignment.all.find_each do |assignment|
      evaluation_target =
        EvaluationTarget.find_or_create_by!(period_id: assignment.period_id,
                                            evaluatee_id: assignment.evaluatee_id)
      assignment.evaluation_target_id = evaluation_target.id
      assignment.save!
    end

    change_column_null :assignments, :evaluation_target_id, false
    add_index :assignments, [:evaluation_target_id, :evaluator_id], unique: true
    remove_column :assignments, :period_id
    remove_column :assignments, :evaluatee_id
  end

  def down
    add_reference :assignments, :period, null: true, foreign_key: true
    add_reference :assignments, :evaluatee, null: true, foreign_key: { to_table: :users }

    Assignment.all.find_each do |assignment|
      evaluation_target = EvaluationTarget.find(assignment.evaluation_target_id)
      assignment.period_id = evaluation_target.period_id
      assignment.evaluatee_id = evaluation_target.evaluatee_id
      assignment.save!
    end

    change_column_null :assignments, :period_id, false
    change_column_null :assignments, :evaluatee_id, false
    remove_column :assignments, :evaluation_target_id
  end
end
