class EnablePgroonga < ActiveRecord::Migration[6.0]
  def change
    reversible do |d|
      d.up do
        enable_extension("pgroonga") unless extension_enabled?("pgroonga")
      end
    end
  end
end
