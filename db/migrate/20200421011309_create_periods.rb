class CreatePeriods < ActiveRecord::Migration[6.0]
  def change
    create_table :periods do |t|
      t.string :name
      t.date :start_on
      t.date :end_on
      t.references :standard, null: false, foreign_key: true

      t.timestamps
    end

    add_index :periods, :start_on
    add_index :periods, :end_on
  end
end
