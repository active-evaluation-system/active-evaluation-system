class CreateEvaluationReports < ActiveRecord::Migration[6.0]
  def change
    create_table :evaluation_reports do |t|
      t.references :period, null: false, foreign_key: true
      t.references :evaluatee, null: false, foreign_key: { to_table: :users }
      t.text :content

      t.timestamps
    end
  end
end
