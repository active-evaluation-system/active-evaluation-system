class AddItemGroupToItem < ActiveRecord::Migration[7.1]
  class ItemTagging < ActiveRecord::Base
    belongs_to :item
    belongs_to :item_tag
  end

  class ItemTag < ActiveRecord::Base
    has_many :item_taggings, dependent: :destroy
    has_many :items, through: :item_taggings
  end

  class ItemGroup < ActiveRecord::Base
  end

  class Item < ActiveRecord::Base
    has_one :item_group
  end

  def up
    add_reference :items, :item_group, foreign_key: true
    ActiveRecord::Base.transaction do
      ItemTag.where(for_group: true).each do |item_tag|
        item_group = ItemGroup.create!(name: item_tag.name)
        item_tag.items.each do |item|
          item.item_group_id = item_group.id
          item.save!
        end
        item_tag.destroy
      end
    end
    change_column_null :items, :item_group_id, false
    remove_column :item_tags, :for_group
  end

  def down
    add_column :item_tags, :for_group, :boolean
    ActiveRecord::Base.transaction do
      ItemGroup.all.each do |item_group|
        item_tag = ItemTag.create!(name: item_group.name, for_group: true)
        item_group.items.each do |item|
          item.item_tags << item_tag
        end
        item_group.destroy
      end
    end
    remove_column :items, :item_group_id
  end
end
