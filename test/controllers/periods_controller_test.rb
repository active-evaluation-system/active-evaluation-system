require 'test_helper'

class PeriodsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @period = periods("2020Q1")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get periods_url
    assert_response :success
  end

  test "should get new" do
    get new_period_url
    assert_response :success
  end

  test "should create period" do
    assert_difference('Period.count') do
      post periods_url, params: { period: { end_on: @period.end_on, name: @period.name, standard_id: @period.standard_id, start_on: @period.start_on } }
    end

    assert_redirected_to period_url(Period.last)
  end

  test "should show period" do
    sign_in users(:alice)
    get period_url(@period)
    assert_response :success
  end

  test "should get edit" do
    get edit_period_url(@period)
    assert_response :success
  end

  test "should update period" do
    patch period_url(@period), params: { period: { end_on: @period.end_on, name: @period.name, standard_id: @period.standard_id, start_on: @period.start_on } }
    assert_redirected_to period_url(@period)
  end

  test "should destroy period" do
    assert_difference('Period.count', -1) do
      delete period_url(@period)
    end

    assert_redirected_to periods_url
  end
end
