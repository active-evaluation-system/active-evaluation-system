require 'test_helper'

class AssignmentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @assignment = assignments("2020Q1-alice-chris")
    sign_in users(:bob)
  end

  test "should create assignment" do
    assert_difference('Assignment.count') do
      post evaluation_target_assignments_url(@assignment.evaluation_target),
           params: {
             assignment: {
               evaluation_target_id: @assignment.evaluation_target_id,
               evaluator_id: users("bob").id,
             }
           }
    end

    assert_redirected_to @assignment.evaluation_target
  end

  test "should destroy assignment" do
    evaluation_target = @assignment.evaluation_target
    assert_difference('Assignment.count', -1) do
      delete assignment_url(@assignment)
    end

    assert_redirected_to evaluation_target
  end
end
