require 'test_helper'

class StandardsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @standard = standards("2020-01")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get standards_url
    assert_response :success
  end

  test "should get new" do
    get new_standard_url
    assert_response :success
  end

  test "should create standard" do
    assert_difference('Standard.count') do
      post standards_url, params: { standard: { name: @standard.name } }
    end

    assert_redirected_to standard_url(Standard.last)
  end

  test "should show standard" do
    sign_in users(:alice)
    get standard_url(@standard)
    assert_response :success
  end

  test "should get edit" do
    get edit_standard_url(@standard)
    assert_response :success
  end

  test "should update standard" do
    i = 0
    standardizations_attributes = {}
    @standard.standardizations.each do |standardization|
      standardizations_attributes[i.to_s] = {
        id: standardization.id.to_s,
        item_id: standardization.item_id.to_s,
        standard_id: standardization.standard_id.to_s,
        _destroy: "1",
      }
      i += 1
    end
    standardizations_attributes[i.to_s] = {
      item_id: items("help-others").id.to_s,
      standard_id: @standard.id.to_s,
    }
    patch standard_url(@standard),
          params: {
            standard: {
              name: @standard.name,
              standardizations_attributes: standardizations_attributes,
            },
          }
    assert_redirected_to standard_url(@standard)
    @standard.reload
    assert_equal([items("help-others")],
                 @standard.items.to_a)
  end

  test "should destroy standard" do
    Period.destroy_all

    assert_difference('Standard.count', -1) do
      delete standard_url(@standard)
    end

    assert_redirected_to standards_url
  end
end
