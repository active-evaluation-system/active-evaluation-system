require 'test_helper'

class EvaluationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @evaluation = evaluations("2020Q1-alice-chris-fix-in-upstream")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get assignment_evaluations_url(@evaluation.assignment)
    assert_response :success
  end

  test "should show evaluation" do
    sign_in users(:alice)
    get evaluation_url(@evaluation)
    assert_response :success
  end

  test "should get edit" do
    get edit_evaluation_url(@evaluation)
    assert_response :success
  end

  test "should update evaluation" do
    patch evaluation_url(@evaluation),
          params: {
            evaluation: {
              assignment_id: @evaluation.assignment_id,
              comment: @evaluation.comment,
              item_id: @evaluation.item_id,
              value: @evaluation.value
            }
          }
    assert_redirected_to period_url(@evaluation.assignment.period)
  end
end
