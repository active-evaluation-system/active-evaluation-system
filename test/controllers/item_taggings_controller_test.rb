require 'test_helper'

class ItemTaggingsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @item_tagging = item_taggings("fix-in-upstream-free-software-level-1")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get item_taggings_url
    assert_response :success
  end

  test "should get new" do
    get new_item_tagging_url
    assert_response :success
  end

  test "should create item_tagging" do
    assert_difference('ItemTagging.count') do
      post item_taggings_url, params: { item_tagging: { item_id: @item_tagging.item_id, item_tag_id: @item_tagging.item_tag_id } }
    end

    assert_redirected_to item_tagging_url(ItemTagging.last)
  end

  test "should show item_tagging" do
    sign_in users(:alice)
    get item_tagging_url(@item_tagging)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_tagging_url(@item_tagging)
    assert_response :success
  end

  test "should update item_tagging" do
    patch item_tagging_url(@item_tagging), params: { item_tagging: { item_id: @item_tagging.item_id, item_tag_id: @item_tagging.item_tag_id } }
    assert_redirected_to item_tagging_url(@item_tagging)
  end

  test "should destroy item_tagging" do
    assert_difference('ItemTagging.count', -1) do
      delete item_tagging_url(@item_tagging)
    end

    assert_redirected_to item_taggings_url
  end
end
