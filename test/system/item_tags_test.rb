require "application_system_test_case"

class ItemTagsTest < ApplicationSystemTestCase
  setup do
    @item_tag = item_tags(:one)
  end

  test "visiting the index" do
    visit item_tags_url
    assert_selector "h1", text: "Item Tags"
  end

  test "creating a Item tag" do
    visit item_tags_url
    click_on "New Item Tag"

    fill_in "Name", with: @item_tag.name
    click_on "Create Item tag"

    assert_text "Item tag was successfully created"
    click_on "Back"
  end

  test "updating a Item tag" do
    visit item_tags_url
    click_on "Edit", match: :first

    fill_in "Name", with: @item_tag.name
    click_on "Update Item tag"

    assert_text "Item tag was successfully updated"
    click_on "Back"
  end

  test "destroying a Item tag" do
    visit item_tags_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Item tag was successfully destroyed"
  end
end
