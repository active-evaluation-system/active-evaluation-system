require "application_system_test_case"

class FinalEvaluationsTest < ApplicationSystemTestCase
  setup do
    @final_evaluation = final_evaluations(:one)
  end

  test "visiting the index" do
    visit final_evaluations_url
    assert_selector "h1", text: "Final Evaluations"
  end

  test "creating a Final evaluation" do
    visit final_evaluations_url
    click_on "New Final Evaluation"

    fill_in "Comment", with: @final_evaluation.comment
    fill_in "Evaluatee", with: @final_evaluation.evaluatee_id
    fill_in "Item", with: @final_evaluation.item_id
    fill_in "Period", with: @final_evaluation.period_id
    fill_in "Value", with: @final_evaluation.value
    click_on "Create Final evaluation"

    assert_text "Final evaluation was successfully created"
    click_on "Back"
  end

  test "updating a Final evaluation" do
    visit final_evaluations_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @final_evaluation.comment
    fill_in "Evaluatee", with: @final_evaluation.evaluatee_id
    fill_in "Item", with: @final_evaluation.item_id
    fill_in "Period", with: @final_evaluation.period_id
    fill_in "Value", with: @final_evaluation.value
    click_on "Update Final evaluation"

    assert_text "Final evaluation was successfully updated"
    click_on "Back"
  end

  test "destroying a Final evaluation" do
    visit final_evaluations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Final evaluation was successfully destroyed"
  end
end
