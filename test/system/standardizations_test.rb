require "application_system_test_case"

class StandardizationsTest < ApplicationSystemTestCase
  setup do
    @standardization = standardizations(:one)
  end

  test "visiting the index" do
    visit standardizations_url
    assert_selector "h1", text: "Standardizations"
  end

  test "creating a Standardization" do
    visit standardizations_url
    click_on "New Standardization"

    fill_in "Item", with: @standardization.item_id
    fill_in "Standard", with: @standardization.standard_id
    click_on "Create Standardization"

    assert_text "Standardization was successfully created"
    click_on "Back"
  end

  test "updating a Standardization" do
    visit standardizations_url
    click_on "Edit", match: :first

    fill_in "Item", with: @standardization.item_id
    fill_in "Standard", with: @standardization.standard_id
    click_on "Update Standardization"

    assert_text "Standardization was successfully updated"
    click_on "Back"
  end

  test "destroying a Standardization" do
    visit standardizations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Standardization was successfully destroyed"
  end
end
